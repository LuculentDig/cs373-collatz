#!/usr/bin/env python3
"""testing the functionalities """

# --------------
# TestCollatz.py
# --------------

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import (
    collatz_read,
    collatz_eval,
    collatz_print,
    collatz_solve,
    collatz_one,
    collatz_brute,
)

# -----------
# TestCollatz
# -----------


class TestCollatz(TestCase):
    """unit tests on functionalities"""

    # ----
    # read
    # ----

    def test_read(self):
        """test read functions"""
        s_input = "1 10\n"
        i, j = collatz_read(s_input)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        """test two small numbers"""
        value = collatz_eval(1, 10)
        self.assertEqual(value, 20)

    def test_eval_2(self):
        """ test two numbers in the hundreds"""
        value = collatz_eval(100, 200)
        self.assertEqual(value, 125)

    def test_eval_3(self):
        """ two numbers within the same hundred range"""
        value = collatz_eval(201, 210)
        self.assertEqual(value, 89)

    def test_eval_4(self):
        """testing from hundreds to thousands"""
        value = collatz_eval(900, 1000)
        self.assertEqual(value, 174)

    def test_eval_5(self):
        """test a very long range"""
        value = collatz_eval(10, 1000001)
        self.assertEqual(value, 525)

    def test_eval_6(self):
        """testing two numbers both in the 100,000s """
        value = collatz_eval(110000, 520000)
        self.assertEqual(value, 470)

    def test_collatz_one_1(self):
        """testing a single number"""
        value = collatz_one(10)
        self.assertEqual(value, 7)

    def test_collatz_one_2(self):
        """test number 1"""
        value = collatz_one(1)
        self.assertEqual(value, 1)

    def test_collatz_brute_1(self):
        """ test collatz_brute function"""
        value = collatz_brute(1, 10)
        self.assertEqual(value, 20)

    # -----
    # print
    # -----

    def test_print(self):
        """test the print functions"""
        writer = StringIO()
        collatz_print(writer, 1, 10, 20)
        print("TESTING??")
        self.assertEqual(writer.getvalue(), "1 10 20\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        """test the solve functions"""
        reader = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        writer = StringIO()
        collatz_solve(reader, writer)
        self.assertEqual(
            writer.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n"
        )


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
