#!/usr/bin/env python3
"""where all the functionalities are"""
# ----------
# Collatz.py
# ----------
from typing import IO, List
import math

# ------------
# collatz_read
# ------------


def collatz_read(input_str: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    input_str = input_str.split()
    return [int(input_str[0]), int(input_str[1])]


# ------------
# collatz_eval
# ------------
CACHE = {1: 1}


def collatz_one(input_num: int) -> int:
    """ returns the sequence of a number"""
    if input_num in CACHE:
        return CACHE[input_num]
    result = 0
    if input_num % 2 == 0:
        result = 1 + collatz_one(input_num >> 1)
    else:
        result = 1 + collatz_one((input_num << 1) + input_num + 1)

    CACHE[input_num] = result
    return result


def collatz_brute(i: int, j: int) -> int:
    """ return the maximum sequence length given a range without cache"""
    max_length = 0
    for ind in range(i, j + 1):
        length = collatz_one(ind)
        if length > max_length:
            max_length = length
    return max_length


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>

    # meta cache step size 100,000
    pre_cache = [351, 383, 443, 441, 449, 470, 509, 504, 525, 507]

    i_temp = min(i, j)
    j_temp = max(i, j)
    i = i_temp
    j = j_temp

    result2 = 0

    whole_i = math.floor(i / 100000)
    whole_j = math.floor(j / 100000)

    if whole_i >= 0 and whole_i <= 9 and whole_j - whole_i > 1:
        # when you need cache
        result2 = collatz_brute(i, whole_i * 100000 + 99999)
        whole_i += 1
        while whole_i < whole_j and whole_i <= 9:
            length = pre_cache[whole_i]
            if length > result2:
                result2 = length
            whole_i += 1

        length = collatz_brute(whole_i * 100000, j)
        if length > result2:
            result2 = length
        return result2
    else:
        # when no cache is needed
        result2 = collatz_brute(i, j)
    return result2


# -------------
# collatz_print
# -------------


def collatz_print(writer: IO[str], i: int, j: int, max_length: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    writer.write(str(i) + " " + str(j) + " " + str(max_length) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(reader: IO[str], writer: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for item in reader:
        i, j = collatz_read(item)
        length = collatz_eval(i, j)
        collatz_print(writer, i, j, length)
